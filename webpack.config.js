var path = require("path");
module.exports = {
    entry: {
        app: ["./main.js"]
    },
    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: "/assets/",
        filename: "bundle.js"
    },
    module: {
       preLoaders: [
           {
             test: /\.js$/,
             exclude: /node_modules/,
             loader: 'jshint-loader'

           }
        ],
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2016']
                }
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.es6']
    },
};

