import $ from 'jquery';


var timer = $('#timer');

function timeThough(e) {
  var i = 1;
  var _now = new Date();
  var _time = [ _now.getHours(), _now.getMinutes(), _now.getSeconds() ];
  var _suffix = ( _time[0] < 12 ) ? "AM" : "PM";
  _time[0] = ( _time[0] < 12 ) ? _time[0] : _time[0] - 12;
  _time[0] = _time[0] || 12;
  for (; i < 3; i++) {
    if ( _time[i] < 10 ) {
      _time[i] = "0" + _time[i];
    }
  }
 
  timer.html(
  _time.join(":") + " " + _suffix
  );
}

var timeoutID = window.setInterval(timeThough, 1000);
